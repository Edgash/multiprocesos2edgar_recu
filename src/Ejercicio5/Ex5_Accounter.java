package Ejercicio5;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

//no se por que no va, lo ejecuto y me hace el calculo bien pero no me lo guarda en 'Totals.txt'
//pero lo vuelvo a ejecutar y me dice que el fichero no contiene números
public class Ex5_Accounter {

    public static void main(String[] args) {
        //comprobamos si los argumentos son mayor que 0
        //if (args.length > 0) {

            List<String> list = Arrays.asList(args);
            //creamos el comando para la ejecución del hijo
            String command = "java -jar out/Ejercicio5_jar/MultiprocesoEdgar2.jar";
            //asignamos el nombre del fichero
            String fichero = "Totals.txt";
            String numeros = "numeros";
            ProcessBuilder pb;

            File entrada = null;
            FileReader lectura = null;
            BufferedReader br = null;
            try {
                entrada = new File("numeros");
                lectura = new FileReader(entrada);
                br = new BufferedReader(lectura);
                String linea, cad = "";
                int sum = 0;
                while ((linea = br.readLine()) != null) {
                    for (int x = 0; x < linea.length(); x++) {
                        if (String.valueOf(linea.charAt(x)).equals(" ")) {
                            sum += Integer.parseInt(cad);
                            cad = "0";
                        } else {
                            cad += linea.charAt(x);
                        }
                    }
                    sum += Integer.parseInt(cad);
                    cad = "0";
                }
                System.out.println("La suma de todos los numeros es : " + sum);
                FileWriter fw=new FileWriter("Totals.txt");
                BufferedWriter bw =new BufferedWriter(fw);
                bw.write(String.valueOf(sum));
                bw.flush();
                System.out.println("Datos guardados en Totals.txt");
            } catch (Exception c) {
                System.out.println("No existe el archivo");
            } finally {
                try {
                    if (null != lectura) {
                        lectura.close();
                    }
                } catch (Exception b) {
                    System.out.println("Error al cerrar el archivo");
                }

            }
    }
}