package Ejercicio5;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class Adder {
    public static void main(String[] args) {

        if (args.length > 0) {
            String fichero = args[0];
            FileReader lectura = null;
            BufferedReader br = null;
            try {
                fichero = String.valueOf(new File("numeros"));
                lectura = new FileReader(fichero);
                br = new BufferedReader(lectura);
                String linea, cad = "";
                int sum = 0;
                while ((linea = br.readLine()) != null) {
                    for (int x = 0; x < linea.length(); x++) {
                        if (String.valueOf(linea.charAt(x)).equals(" ")) {
                            sum += Integer.parseInt(cad);
                            cad = "0";
                        } else {
                            cad += linea.charAt(x);
                        }
                    }
                    sum += Integer.parseInt(cad);
                    cad = "0";
                }
                System.out.println("La suma de todos los numeros es: " + sum);
            } catch (Exception c) {
                System.out.println("No existe el archivo");
            } finally {
                try {
                    if (null != lectura) {
                        lectura.close();
                    }
                } catch (Exception b) {
                    System.out.println("Error al cerrar el archivo");
                }
            }//si no se cumple lo anterior mostrará un mensaje
        }else{
        System.out.println("No se han pasado argumentos...");
    }
    }
}
