package Ejercicio4;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


public class Ejercicio4 {
    private static List<String> traduccion;
    private static String diccionario;
    private static String fichero;

    public static void main(String[] args) {

        traduccion = new ArrayList<>();

        String command = "java -jar out/Ejercicio4_jar/MultiprocesoEdgar2.jar";

        List<String> lista = new ArrayList<>(Arrays.asList(command.split(" ")));

        if (diccionario == null) {
        }else{
            traduccion.add(diccionario);
        }

        try {
            ProcessBuilder pb = new ProcessBuilder(lista);
            Process process = pb.start();
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));

            if (fichero != null) {

                BufferedReader br = new BufferedReader(new FileReader(fichero));
                String original = br.readLine();

                while (original != null) {
                    bw.write(original);
                    bw.flush();

                    original = br.readLine();
                }
                br.close();
            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }


}