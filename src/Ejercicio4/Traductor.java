package Ejercicio4;

import java.io.*;
import java.util.HashMap;
import java.util.Scanner;

public class Traductor {
    private static HashMap<String, String> hm;

    private static String archivo = "Translations";

    public static void main(String[] args) {
        System.out.println("Digame una palabra a traducir");
        Scanner sc = new Scanner(System.in);
        String linea;
        linea = sc.nextLine();
        hm = new HashMap<>();
        lista(args);

        while (!linea.equals("0")) {

            if (archivo.contains(linea)) {
                separarPalabras(String.valueOf(sc));
            } else {
                System.out.println("Desconocido");
            }
            linea = sc.nextLine();
        }
        sc.close();
        }

    public static void separarPalabras(String linea){
        String[] traduccion = linea.split(";");
        hm.put(traduccion[0], traduccion[0]);
        System.out.println(traduccion[0]);
    }

        public static void lista(String[] lista){

            if (lista.length > 0) {
                archivo = lista[0];
            }
            try{
                File f = new File(archivo);
                BufferedReader br = new BufferedReader(new FileReader(f));
                String linea = br.readLine();

                while(linea != null) {
                    separarPalabras(linea);
                    linea = br.readLine();
                }
                br.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
}
